(function() {
  var $, ANIMATION_DURATION, MAX_PROGRESS_DURATION, MIN_PROGRESS_DURATION, PROGRESS_ANIMATION_DURATION;

  ANIMATION_DURATION = 700;

  MIN_PROGRESS_DURATION = 1;

  MAX_PROGRESS_DURATION = 1500;

  PROGRESS_ANIMATION_DURATION = 2000;

  $ = jQuery;

  $.fn.disableSelection = function() {
    return this.each(function() {
      var $this;
      $this = $(this);
      $this.attr('unselectable', 'on');
      $this.css({
        '-moz-user-select': 'none',
        '-webkit-user-select': 'none',
        'user-select': 'none'
      });
      return $this.each(function() {
        return this.onselectstart = function() {
          return false;
        };
      });
    });
  };

  $.fn.enableSelection = function() {
    return this.each(function() {
      var $this;
      $this = $(this);
      $this.attr('unselectable', '');
      $this.css({
        '-moz-user-select': '',
        '-webkit-user-select': '',
        'user-select': ''
      });
      return $this.each(function() {
        return this.onselectstart = null;
      });
    });
  };

  $.extend(REPLIT, {
    last_progress_ratio: 0,
    InitDOM: function() {
      this.$doc_elem = $('html');
      this.$container = $('#main');
      this.$editorContainer = $('#editor');
      this.$consoleContainer = $('#console');
      
      this.$console = this.$consoleContainer.find('.jqconsole');
      this.$editor = this.$editorContainer.find('#editor-widget');

      this.$progress = $('#progress');
      this.$progressFill = $('#progress-fill');

      this.$run = $('#editor-run');
      this.ResizeWorkspace();
    },
    
    OnProgress: function(percentage) {
      var duration, fill, ratio;
      ratio = percentage / 100.0;
      if (ratio < this.last_progress_ratio) {
        return;
      }
      duration = (ratio - this.last_progress_ratio) * PROGRESS_ANIMATION_DURATION;
      this.last_progress_ratio = ratio;
      duration = Math.max(duration, MIN_PROGRESS_DURATION);
      duration = Math.min(duration, MAX_PROGRESS_DURATION);
      fill = this.$progressFill;
      return fill.animate({
        width: percentage + '%'
      }, {
        duration: Math.abs(duration),
        easing: 'linear',
        step: function(now, fx) {
          var blue_bottom, blue_top, bottom, green_bottom, green_top, red_bottom, red_top, top;
          ratio = now / 100.0;
          red_top = Math.round(ratio < 0.75 ? 250 : 250 + (199 - 250) * ((ratio - 0.75) / 0.25));
          red_bottom = Math.round(ratio < 0.75 ? 242 : 250 + (136 - 250) * ((ratio - 0.75) / 0.25));
          green_top = Math.round(ratio < 0.25 ? 110 + (181 - 110) * (ratio / 0.25) : 181 + (250 - 181) * ((ratio - 0.25) / 0.75));
          green_bottom = Math.round(34 + (242 - 34) * ratio);
          blue_top = 67;
          blue_bottom = 12;
          top = "rgb(" + red_top + ", " + green_top + ", " + blue_top + ")";
          bottom = "rgb(" + red_bottom + ", " + green_bottom + ", " + blue_bottom + ")";
          if ($.browser.webkit) {
            fill.css({
              'background-image': "url('/images/progress.png'), -webkit-gradient(linear, left top, left bottom, from(" + top + "), to(" + bottom + "))"
            });
          } else if ($.browser.mozilla) {
            fill.css({
              'background-image': "url('/images/progress.png'), -moz-linear-gradient(top, " + top + ", " + bottom + ")"
            });
          } else if ($.browser.opera) {
            fill.css({
              'background-image': "url('/images/progress.png'), -o-linear-gradient(top, " + top + ", " + bottom + ")"
            });
          }
          return fill.css({
            'background-image': "url('/images/progress.png'), linear-gradient(top, " + top + ", " + bottom + ")"
          });
        }
      });
    },
    ResizeWorkspace: function() {
      var editor_width = 500;
      var console_width = 450;

      this.$editorContainer.css({
        width: editor_width,
        height: 640
      });
      this.$consoleContainer.css({
        width: console_width,
        height: 640
      });

      this.$editor.css('width', editor_width);
      this.$editor.css('height', 640);

      //return this.editor.resize();
    }
  });

  $(function() {
    REPLIT.$this.bind('language_loading', function(_, system_name) {
      var $about, $engine, $links, lang;
      REPLIT.$progress.animate({
        opacity: 1
      }, 'fast');
      REPLIT.$progressFill.css({
        width: 0
      });
      REPLIT.last_progress_ratio = 0;
      lang = REPLIT.Languages[system_name.toLowerCase()];
      return;
    });
    REPLIT.$this.bind('language_loaded', function(e, lang_name) {
      REPLIT.OnProgress(100);
      REPLIT.$progress.animate({
        opacity: 0
      }, 'fast');
      return;
    });
    
    console.log("REPL DOM init");
    REPLIT.InitDOM();
  });

}).call(this);
