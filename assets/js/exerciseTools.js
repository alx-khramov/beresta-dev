function loadEditorCode (editorCode) {
  REPLIT.editor.setValue(editorCode);
}

ExerciseTools = {

  outputClassCounter: 0,
  outputClass: "output0",

  init: function (exerciseId, progLangId, editorCode) {
    this.exerciseId = exerciseId;
    this.progLangId = progLangId;
    this.editorCode = editorCode;

    $( window ).load(function() {
      console.log('REPL init');
      REPLIT.Init();

      if ( progLangId === 1) {
        REPLIT.LoadLanguage('javascript', function(){loadEditorCode(editorCode)});
      }
      else if ( progLangId === 2) {
        REPLIT.LoadLanguage('ruby',  function(){loadEditorCode(editorCode)});
      }
      else if ( progLangId === 3) {
        REPLIT.LoadLanguage('python',  function(){loadEditorCode(editorCode)});
      }
    });
  },

  checkSolution: function () {
    $(".exercise-coding-bar_solved-msg").hide();
    $(".exercise-coding-bar_unsolved-msg").hide();

    var self = this;
    var selector = "."+this.outputClass+" > span";    

    var checker = setInterval(function () {
      var output = $(selector).text();

      if (output) {
        clearInterval(checker);

        output = output.substr(0, output.length-1);
        output = output.replace(/\n/g, "\r\n");

        $.ajax({
          type: "POST",
          url: "/exercise/check/",
          data: {id: self.exerciseId, solution: output}
        })
        .fail(function() {
          console.log("Solution checking failed");
        })
        .done(function( res ) {
          if (res.solved)
            $(".exercise-coding-bar_solved-msg").fadeIn("slow");
          else
            $(".exercise-coding-bar_unsolved-msg").fadeIn("slow");
        });

        self.outputClassCounter++;
        self.outputClass = "output"+self.outputClassCounter;
      }

    }, 1000);

  }
}