module.exports = {

  mismatchPasswords: {
    customError: [{ message: 'Пароли не совпадают.' }]
  },

  noSuchUser: {
    customError: [{ message: 'Такого пользователя не существует.' }]
  },

  duplicateUser: { 
    customError: [{ message: 'Пользователь с такими данными уже существует.' }]
  },

  duplicateCourse: { 
    customError: [{ message: 'Курс с таким названием уже существует.' }]
  },

  duplicateExercise: { 
    customError: [{ message: 'Задание с таким заголовком уже существует.' }]
  },

  loginAndPasswordRequired: {
    customError: [{ message: 'Необходимо ввести логин и пароль.' }]
  },

  loginAndPasswordIncorrect: {
    customError: [{ message: 'Введена неверная комбинация логина и пароля.' }]
  },

  authenticationRequired: {
    customError: [{ message: 'Вы должны быть авторизованы.' }]
  },

  accessDenied: {
    customError: [{ message: 'Доступ запрещен.' }]
  },

  generalError: {
    customError: [{ message: 'Произошла ошибка.' }]
  }

}