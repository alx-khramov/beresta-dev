/**
 * teacherAuthenticated
 *
 * @module      :: Policy
 * @description :: Authenticated teacher.
 *                 Assumes that login action in one of controllers sets `req.session.teacherAuthenticated = true;`
 */

var errorList = require("../../config/errorList");

module.exports = function(req, res, ok) {

  if (req.session.teacherAuthenticated) {
    return ok();
  }
  else {
    req.session.flash = {
      err: errorList.authenticationRequired
    }
    res.redirect('/teacher/signin');
  }
};
