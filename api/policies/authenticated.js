/**
 * authenticated
 *
 * @module      :: Policy
 * @description :: Authenticated user.
 *                 Assumes that login action in one of controllers sets `req.session.authenticated = true;`
 */

var errorList = require("../../config/errorList");

module.exports = function(req, res, ok) {

  if (req.session.authenticated) {
    return ok();
  }
  else {
    req.session.flash = {
      err: errorList.authenticationRequired
    }
    res.redirect('/user/signin');
  }
};
