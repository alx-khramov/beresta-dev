/**
 * Allow a logged-in user to see, edit and update his own profile
 * Allow admins to see everyone
 */

var errorList = require("../../config/errorList");

module.exports = function(req, res, ok) {

  if (!req.session.User) {
    req.session.flash = {
      err: errorList.accessDenied
    }
    res.redirect('/user/signin');
    return;
  }

  var sessionUserMatchesId = req.session.User.id == req.param('id');

  if (!(sessionUserMatchesId)) {
    req.session.flash = {
      err: errorList.accessDenied
    }
    res.redirect('/user/signin');
    return;
  }

  ok();

};