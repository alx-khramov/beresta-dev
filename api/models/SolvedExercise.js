/**
 * SolvedExercise
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  tableName: 'solvedExercises',

  attributes: {
  	
  	userId: {
      type: 'integer',
      required: true
    },

    exerciseId: {
      type: 'integer',
      required: true
    },

    courseId: {
      type: 'integer',
      required: true
    }
    
  }

};
