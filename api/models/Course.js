/**
 * Course
 *
 * @module      :: Model
 * @description :: Course model.
 */

module.exports = {

  tableName: 'courses',

  attributes: {

    name: {
      type: 'string',
      required: true,
      unique: true
    },

    description: {
      type: 'text'
    },

    progLangId: {
      type: 'integer',
      required: true
    }
  	    
  },

  validationMessages: {
    name: {
      required: "Необходимо заполнить название курса."
    },
    progLangId: {
      required: "Необходимо выбрать язык программирования."
    }
  }

};
