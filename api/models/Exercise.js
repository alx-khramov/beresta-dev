/**
 * Exercise
 *
 * @module      :: Model
 * @description :: Exercise model.
 */

module.exports = {

  tableName: 'exercises',

  attributes: {

    title: {
      type: 'string',
      required: true,
      unique: true
    },

    text: {
      type: 'text'
    },

    instructions: {
      type: 'text'
    },

    precode: {
      type: 'text'
    },

    solution: {
      type: 'string',
      required: true
    },

    courseId: {
      type: 'integer',
      required: true
    },

    nextExerciseId: {
      type: 'integer'
    }
        
  },

  validationMessages: {
    title: {
      required: "Необходимо заполнить заголовок задания."
    },
    solution: {
      required: "Необходимо заполнить решение."
    },
    courseId: {
      required: "Необходимо выбрать курс."
    }
  }

};
