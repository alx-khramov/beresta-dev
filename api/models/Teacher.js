/**
 * Teacher
 *
 * @module      :: Model
 * @description :: Teacher model.
 */

var errorList = require("../../config/errorList.js");

module.exports = {

  tableName: 'teachers',

  attributes: {
  	
  	login: {
      type: 'string',
      required: true,
      unique: true
    },

    email: {
      type: 'string',
      email: true
    },

    encryptedPassword: {
      type: 'string'
    }
    
  },

  validationMessages: {
    login: {
      required: "Необходимо заполнить логин."
    },
    email: {
      email: "Электронная почта введена неверно."
    }
  },

  beforeCreate: function (values, next) {

    if (!values.password || values.password != values.confirmation) {
      return next( errorList.mismatchPasswords );
    }

    require('bcrypt').hash(values.password, 10, function passwordEncrypted(err, encryptedPassword) {
      if (err) {
        console.log("! " + JSON.stringify(err));
        return next( errorList.generalError );
      }
      values.encryptedPassword = encryptedPassword;
      next();
    });
  },

  beforeUpdate: function (values, next) {

    if (values.password || values.confirmation){
      if (!values.password || values.password != values.confirmation) {
        return next( errorList.mismatchPasswords );
      }

      require('bcrypt').hash(values.password, 10, function passwordEncrypted(err, encryptedPassword) {
        if (err) {
          console.log("! " + JSON.stringify(err));
          return next( errorList.generalError );
        }
        values.encryptedPassword = encryptedPassword;
        next();
      });
    }

    next();

  }

};
