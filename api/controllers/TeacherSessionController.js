/**
 * teacherSessionController
 *
 * @module      :: Controller
 * @description	:: teacher sessions.
 */

var errorList = require("../../config/errorList.js");

var bcrypt = require('bcrypt');

module.exports = {

  create: function(req, res, next) {

    if (!req.param('login') || !req.param('password')) {
      req.session.flash = {
        err: errorList.loginAndPasswordRequired
      }

      res.redirect('/teacher/signin');
      return;
    }

    Teacher.findOneByLogin( req.param('login'), function foundteacher(err, teacher) {
      if (err) {
          console.log("! " + JSON.stringify(err));
          return next( errorList.generalError );
      }

      if (!teacher) {
        req.session.flash = {
          err: errorList.noSuchUser
        }
        res.redirect('/teacher/signin');
        return;
      }

      bcrypt.compare(req.param('password'), teacher.encryptedPassword, function(err, valid) {
        if (err) {
          console.log("! " + JSON.stringify(err));
          return next( errorList.generalError );
        }

        if (!valid) {
          req.session.flash = {
            err: errorList.loginAndPasswordIncorrect
          }
          res.redirect('/teacher/signin');
          return;
        }

        req.session.teacherAuthenticated = true;
        req.session.teacher = teacher;

        res.redirect('/dashboard/show/');
      });
    });
  },

  destroy: function(req, res, next) {

        req.session.destroy();

        res.redirect('/');
  },


  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to SessionController)
   */
  _config: {}

  
};
