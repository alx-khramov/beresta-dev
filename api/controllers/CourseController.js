/**
 * CourseController
 *
 * @module      :: Controller
 * @description	:: Courses
 * 
 */

var errorList = require("../../config/errorList.js");

module.exports = {
    
  show: function (req, res, next) {
    Course.findOneById( req.param('id'), function (err, course) {
      if (err) {
          console.log("! " + JSON.stringify(err));
          return next( errorList.generalError );
      }
      if (!course) res.send(404);

      Exercise.find({ courseId: course.id}).exec(function(err, exercises){
        if (err) {
            console.log("! " + JSON.stringify(err));
            return next( errorList.generalError );
        }

        SolvedExercise.find({ courseId: course.id, userId: req.session.User.id }).exec(function(err, solvedExercises){
          if (err) {
              console.log("! " + JSON.stringify(err));
              return next( errorList.generalError );
          }

          res.view({
            course: course, exercises: exercises, solvedExercises: solvedExercises
          });

        });
      });

    });
  },

  list: function (req, res, next) {
    Course.find().exec(function(err, courses){
      if (err) {
        console.log("! " + JSON.stringify(err));
        return next( errorList.generalError );
      }

      var lock = courses.length * 2;
      var c = [];

      Object.keys(courses).forEach(function(course) {

        courses[course].exercisesQuantity = 0;
        courses[course].solvedExercisesQuantity = 0;
        c.push(courses[course]);

        Exercise.count({courseId: courses[course].id}).exec(function(err, exercisesQuantity){
          c[course].exercisesQuantity = exercisesQuantity;
          lock = lock - 1;
        });

        SolvedExercise.count({userId: req.session.User.id, courseId: courses[course].id}).exec(function(err, solvedExercisesQuantity){
          c[course].solvedExercisesQuantity = solvedExercisesQuantity;
          lock = lock - 1;
        });

      });
        
      var t = setInterval(function(){
        if (lock == 0) {
          clearInterval(t);
          return res.view({courses: c});
        }
      }, 1000);

    });

  },

  add: function (req, res, next) {
    res.view({ layout: 'dashboardLayout' });
  },

  create: function(req, res, next) {

    var courseObj = {
      name: req.param('course_name'),
      description: req.param('course_description'),
      progLangId: req.param('course_proglangid')
    }

    Course.create(courseObj, function courseCreated(err, course) {
      if (err) {
        var error;

        if (err.ValidationError) {          
          error = transformValidationMsg(Course, err.ValidationError);
        }
        else if (err.customError) {
          error = err;
        }
        else if (err.code == 'ER_DUP_ENTRY') {
          error = errorList.duplicateCourse;
        }
        else {
          console.log("! " + JSON.stringify(err));
          error = errorList.generalError;
        }

        req.session.flash = {
          err: error
        }

        return res.redirect('/course/add');
      }

      req.session.flash = {
        successMessage: 'Курс успешно создан.'
      }

      res.redirect('/exercise/add/?course_id='+course.id);
    });
  },

  edit: function(req, res, next) {
    if (!req.param('id')){
      Course.find().exec(function(err, courses){
        if (err) {
          console.log("! " + JSON.stringify(err));
          return next( errorList.generalError );
        }

        res.view('course/select', {courses: courses, layout: 'dashboardLayout'});
      });
    } 
    else 
    {
      Course.findOneById( req.param('id'), function (err, course) {
        if (err) {
            console.log("! " + JSON.stringify(err));
            return next( errorList.generalError );
        }
        res.view({
          course: course, layout: 'dashboardLayout'
        });
      });
    }
  },

  update: function(req, res, next) {
    var courseObj = {
      name: req.param('course_name'),
      description: req.param('course_description'),
      progLangId: req.param('course_proglangid')
    }

    Course.update(req.param('id'), courseObj, function courseUpdated(err) {
      if (err) {
        var error;

        if (err.ValidationError) {          
          error = transformValidationMsg(Course, err.ValidationError);
        }
        else if (err.customError) {
          error = err;
        }
        else {
          console.log("! " + JSON.stringify(err));
          error = errorList.generalError;
        }

        console.log("Error " + JSON.stringify(error) + "\n" + err);

        req.session.flash = {
          err: error
        }
        
        return res.redirect('/course/edit/' + req.param('id'));
      }

      req.session.flash = {
        successMessage: 'Учебный курс успешно отредактирован.'
      }

      res.redirect('/course/edit/' + req.param('id'));
    });

  },

  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to CourseController)
   */
  _config: {}

  
};
