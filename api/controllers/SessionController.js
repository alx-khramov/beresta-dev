/**
 * SessionController
 *
 * @module      :: Controller
 * @description	:: Sessions
 *
 */

var errorList = require("../../config/errorList.js");

var bcrypt = require('bcrypt');

module.exports = {

  create: function(req, res, next) {

    if (!req.param('login') || !req.param('password')) {
      req.session.flash = {
        err: errorList.loginAndPasswordRequired
      }

      res.redirect('/user/signin');
      return;
    }

    User.findOneByLogin( req.param('login'), function foundUser(err, user) {
      if (err) {
          console.log("! " + JSON.stringify(err));
          return next( errorList.generalError );
      }

      if (!user) {
        req.session.flash = {
          err: errorList.noSuchUser
        }
        res.redirect('/user/signin');
        return;
      }

      bcrypt.compare(req.param('password'), user.encryptedPassword, function(err, valid) {
        if (err) {
          console.log("! " + JSON.stringify(err));
          return next( errorList.generalError );
        }

        if (!valid) {
          req.session.flash = {
            err: errorList.loginAndPasswordIncorrect
          }
          res.redirect('/user/signin');
          return;
        }

        req.session.authenticated = true;
        req.session.User = user;

        res.redirect('/user/show/' + user.id);
      });
    });
  },

  destroy: function(req, res, next) {

        req.session.destroy();

        res.redirect('/user/signin');
  },


  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to SessionController)
   */
  _config: {}

  
};
