/**
 * DashboardController
 *
 * @module      :: Controller
 * @description	:: Administration panel.
 */

module.exports = {

  show: function (req, res) {
    res.view({ layout: 'dashboardLayout' });
  },


  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to AdminController)
   */
  _config: {}

  
};

