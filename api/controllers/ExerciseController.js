/**
 * ExerciseController
 *
 * @module      :: Controller
 * @description :: Exercises
 * 
 */

var errorList = require("../../config/errorList.js");
var SolvedExerciseController = require("./SolvedExerciseController.js");

module.exports = {
    
  show: function (req, res, next) {
    Exercise.findOneById( req.param('id'), function (err, exercise) {
      if (err) {
        console.log("! " + JSON.stringify(err));
        return next( errorList.generalError );
      }
      if (!exercise) res.send(404)
      else {
        Course.findOneById( exercise.courseId, function (err, course) {
          if (err) {
            console.log("! " + JSON.stringify(err));
            return next( errorList.generalError );
          }
          
          res.view({ course: course, exercise: exercise });
        });
      };
    });
  },

  add: function (req, res, next) {
    if (req.param('course_id')) {
      Course.findOneById( req.param('course_id'), function (err, course) {
        if (err) {
          console.log("! " + JSON.stringify(err));
          return next( errorList.generalError );
        }

        res.view({ course_id: course.id, course_name: course.name, layout: 'dashboardLayout' });
      });
    }

    Course.find().exec(function(err, courses){
      if (err) {
          console.log("! " + JSON.stringify(err));
          return next( errorList.generalError );
      }
      else if (!courses) {
        res.redirect('/course/add/');
      }

      res.view({ course_id: null, course_name: null, courses: courses, layout: 'dashboardLayout' });
    });

  },

  create: function(req, res, next) {

    var updatePrevExercise = function(nextExercise) {
      Exercise.findOne({ id: {'!' : nextExercise.id}, courseId: nextExercise.courseId, nextExerciseId: null }).exec(function (err, exercise) {
        if (exercise) {
          exercise.nextExerciseId = nextExercise.id;
          exercise.save(function(err) {
            if (err) {
              console.log("! " + JSON.stringify(err));
              return next( errorList.generalError );
            };
          });
        }
        else {
          return;
        }
      });
    }

    var exerciseObj = {
      title: req.param('exercise_title'),
      text: req.param('exercise_text'),
      instructions: req.param('exercise_instructions'),
      precode: req.param('exercise_precode'),
      solution: req.param('exercise_solution'),
      courseId: req.param('course_id')
    }

    Exercise.create(exerciseObj, function exerciseCreated(err, exercise) {
      if (err) {
        var error;

        if (err.ValidationError) {          
          error = transformValidationMsg(Exercise, err.ValidationError);
        }
        else if (err.customError) {
          error = err;
        }
        else if (err.code == 'ER_DUP_ENTRY') {
          error = errorList.duplicateExercise;
        }
        else {
          console.log("! " + JSON.stringify(err));
          error = errorList.generalError;
        }

        req.session.flash = {
          err: error
        }

        return res.redirect('/exercise/add');
      }

      updatePrevExercise(exercise);

      req.session.flash = {
        successMessage: 'Задание успешно создано.'
      }

      res.redirect('/exercise/add/?course_id=' + exercise.courseId);
    });
  },

  check: function(req, res, next) {
    Exercise.findOneById( req.param('id'), function (err, exercise) {
      if (err) {
        console.log("! " + JSON.stringify(err));
        return next( errorList.generalError );
      }
      if (!exercise) {
        return res.send( "noExercise" );
      }
      if (exercise && (req.param('solution') === decodeURIComponent(exercise.solution))) {
        SolvedExerciseController.add(req.session.User.id, exercise.id, exercise.courseId);
        return res.send( {solved:true} );
      }

      return res.send( {solved:false} );
    });
  },

  courseSelect: function(req, res, next) {
    if (req.param('id')) {
      Exercise.find({ courseId: req.param('id')}).exec(function(err, exercises){
        if (err) {
            console.log("! " + JSON.stringify(err));
            return next( errorList.generalError );
        }

        res.view('exercise/courseShowExercises', { layout: 'dashboardLayout', exercises: exercises })
      });
    }
    else {
      Course.find().exec(function(err, courses){
        if (err) {
          console.log("! " + JSON.stringify(err));
          return next( errorList.generalError );
        }

        res.view('exercise/courseSelect', { layout: 'dashboardLayout', courses: courses });
      });
    }

    
  },

  edit: function(req, res, next) {
    Exercise.findOneById( req.param('id'), function (err, exercise) {
      if (err) {
        console.log("! " + JSON.stringify(err));
        return next( errorList.generalError );
      }

      Course.find().exec(function(err, courses){
        if (err) {
          console.log("! " + JSON.stringify(err));
          return next( errorList.generalError );
        }

        res.view({ exercise: exercise, courses: courses });
      });
    });
    
  },

  update: function(req, res, next) {
    var exerciseObj = {
      title: req.param('exercise_title'),
      text: req.param('exercise_text'),
      instructions: req.param('exercise_instructions'),
      precode: req.param('exercise_precode'),
      solution: req.param('exercise_solution'),
    }

    if (req.param('course_id'))   
      exerciseObj.courseId = req.param('course_id');

    Exercise.update(req.param('id'), exerciseObj, function exerciseUpdated(err) {
      if (err) {
        var error;

        if (err.ValidationError) {          
          error = transformValidationMsg(Exercise, err.ValidationError);
        }
        else if (err.customError) {
          error = err;
        }
        else if (err.code == 'ER_DUP_ENTRY') {
          error = errorList.duplicateExercise;
        }
        else {
          console.log("! " + JSON.stringify(err));
          error = errorList.generalError;
        }

        req.session.flash = {
          err: error
        }
      }
      else {
        req.session.flash = {
          successMessage: 'Задание успешно отредактировано.'
        }
      }
      res.redirect('/exercise/edit/' + req.param('id'));
    });
  },


  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to CourseController)
   */
  _config: {}

  
};
