/**
 * UserController
 *
 * @module      :: Controller
 * @description	:: Users
 *
 */

var errorList = require("../../config/errorList.js");

module.exports = {
    
  signup: function (req, res) {
    res.view();
  },

  create: function(req, res, next) {

    var userObj = {
      login: req.param('login'),
      name: req.param('name'),
      surname: req.param('surname'),
      email: req.param('email'),
      password: req.param('password'),
      confirmation: req.param('confirmation')
    }

    User.create(userObj, function userCreated(err, user) {
      if (err) {
        var error;

        if (err.ValidationError) {          
          error = transformValidationMsg(User, err.ValidationError);
        }
        else if (err.customError) {
          error = err;
        }
        else if (err.code == 'ER_DUP_ENTRY') {
          error = errorList.duplicateUser;
        }
        else {
          console.log("! " + JSON.stringify(err));
          error = errorList.generalError;
        }

        req.session.flash = {
          err: error
        }
        return res.redirect('/user/signup');
      }

      req.session.authenticated = true;
      req.session.User = user;

      res.redirect('/user/show/' + user.id);
    });
  },

  edit: function (req, res, next) {
    User.findOneById( req.param('id'), function (err, user) {
      if (err) {
          console.log("! " + JSON.stringify(err));
          return next( errorList.generalError );
      }
      if (!user) return next( errorList.noSuchUser );
      res.view({
        user: user
      });
    });
  },

  update: function(req, res, next) {
    var userObj = {
      name: req.param('name'),
      surname: req.param('surname'),
      email: req.param('email'),
      password: req.param('password'),
      confirmation: req.param('confirmation')
    }

    User.update(req.param('id'), userObj, function userUpdated(err) {
      if (err) {
        var error;

        if (err.ValidationError) {          
          error = transformValidationMsg(User, err.ValidationError);
        }
        else if (err.customError) {
          error = err;
        }
        else {
          console.log("! " + JSON.stringify(err));
          error = errorList.generalError;
        }

        req.session.flash = {
          err: error
        }
        
        return res.redirect('/user/edit/' + req.param('id'));
      }

      res.redirect('/user/show/' + req.param('id'));
    });
  },

  show: function (req, res, next) {
    User.findOneById( req.param('id'), function (err, user) {
      if (err) {
          console.log("! " + JSON.stringify(err));
          return res.send( errorList.generalError );
      }
      if (!user) return res.send( errorList.noSuchUser );

      var sessionUserMatchesId = false;
      if (req.session.User)
        sessionUserMatchesId = (req.session.User.id == req.param('id'));

      res.view({
        user: user, sessionUserMatchesId: sessionUserMatchesId
      });
    });
  },

  signin: function(req, res) {
    res.view();
  },


  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to UserController)
   */
  _config: {}

  
};
