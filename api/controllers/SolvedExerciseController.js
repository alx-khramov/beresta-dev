/**
 * SolvedExerciseController
 *
 * @module      :: Controller
 * @description	:: Solved exercises and courses.
 */

var errorList = require("../../config/errorList.js");

module.exports = {
  
  add: function (userId, exerciseId, courseId) {
    var solvedExerciseObj = {
      userId: userId,
      exerciseId: exerciseId,
      courseId: courseId
    }

    SolvedExercise.findOne(solvedExerciseObj).exec(function(err, solvedExercise){
      if (err) {
          console.log("! " + JSON.stringify(err));
          return;
      }
      if (!solvedExercise) {
        SolvedExercise.create(solvedExerciseObj, function solvedExerciseCreated(err, solvedExercise) {
          if (err) {
            console.log("! " + JSON.stringify(err));
          }
          return;
        });
      }

    });

  },
  


  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to SolvedExerciseController)
   */
  _config: {}

  
};
