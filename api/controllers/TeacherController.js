/**
 * TeacherController
 *
 * @module      :: Controller
 * @description	:: Teachers
 */

var errorList = require("../../config/errorList.js");

module.exports = {
    
  signin: function(req, res) {
    res.view({ layout: 'dashboardLayout' });
  },

  add: function(req, res) {
    res.view({ layout: 'dashboardLayout' });
  },

  create: function(req, res, next) {

    var teacherObj = {
      login: req.param('login'),
      email: req.param('email'),
      password: req.param('password'),
      confirmation: req.param('confirmation')
    }

    Teacher.create(teacherObj, function teacherCreated(err, teacher) {
      if (err) {
        var error;

        if (err.ValidationError) {          
          error = transformValidationMsg(User, err.ValidationError);
        }
        else if (err.customError) {
          error = err;
        }
        else if (err.code == 'ER_DUP_ENTRY') {
          error = errorList.duplicateUser;
        }
        else {
          console.log("! " + JSON.stringify(err));
          error = errorList.generalError;
        }

        req.session.flash = {
          err: error
        }

        return res.redirect('/teacher/add');
      }

      req.session.flash = {
        successMessage: 'Новый профиль преподавателя успешно создан.'
      }

      res.redirect('/teacher/add/');
    });
  },

  edit: function (req, res, next) {
    teacher.findOneById( req.param('id'), function (err, teacher) {
      if (err) {
          console.log("! " + JSON.stringify(err));
          return next( errorList.generalError );
      }
      if (!teacher) return next( errorList.noSuchUser );
      res.view({
        teacher: teacher, layout: 'dashboardLayout'
      });
    });
  },

  update: function(req, res, next) {
    var teacherObj = {
      email: req.param('email'),
      password: req.param('password'),
      confirmation: req.param('confirmation')
    }

    teacher.update(req.param('id'), teacherObj, function userUpdated(err) {
      if (err) {
        req.session.flash = {
          err: err
        }

        return res.redirect('/teacher/edit/' + req.param('id'));
      }

      res.redirect('/teacher/edit/');
    });
  },

  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to teacherController)
   */
  _config: {}

  
};
